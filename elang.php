<?php

class Elang extends Hewan
{
    use Fight;
    public $jenisHewan = "Elang";
    public function __construct($nama_elang) {
        $this->nama = $nama_elang;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan() {
        echo "<pre>". print_r($this, true) ."</pre>";
        echo "<br><br>";
    }

    public function atraksi()
    {
        echo "$this->nama sedang $this->keahlian";
        echo "<br><br>";
    }
}

?>
