<?php 

class Harimau extends Hewan
{
    use Fight;
    public $jenisHewan = "Harimau";
    public function __construct($nama_harimau) {
        $this->nama = $nama_harimau;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan() {
        echo "<pre>". print_r($this, true) ."</pre>";
        echo "<br><br>";
    }

    public function atraksi()
    {
        echo "$this->nama sedang $this->keahlian";
        echo "<br><br>";
    }
}

 ?>