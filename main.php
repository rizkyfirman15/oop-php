<?php

abstract class Hewan
{
    public $nama, 
        $jumlahKaki, 
        $keahlian, 
        $darah = 50.0;

    abstract public function atraksi();
}

trait Fight
{
    public $attackPower, $defencePower;

    public function serang($hewan){
        echo "$this->nama menyerang $hewan->nama";
        echo "<br>";
        $hewan->diserang($this);
    }
    
    public function diserang($hewan){
        echo "Warning! $this->nama diserang ". $hewan->nama;
        echo "<br>";
        $chance = random_int(0,10);
        if ($chance > 8) {
            echo "Lucky! Serangan meleset.";
        }

        else {
            $damage = floatval($hewan->attackPower/$this->defencePower);
            $this->darah = floatval($this->darah) - $damage;
            echo "Damage $damage diterima! ";
            if ($this->darah > 0) {
                echo "$this->nama sisa darah $this->darah";
            }
            else {
                echo "$this->nama telah K.O.!";
            }
        }

        echo "<br><br>";
    }
}

 ?>